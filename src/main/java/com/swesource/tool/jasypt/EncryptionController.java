package com.swesource.tool.jasypt;

import com.swesource.tool.jasypt.model.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class EncryptionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionController.class);
    private static final Encryptor ENCRYPTOR = new Encryptor();
    private static final String PAGE_INDEX = "index";

    @GetMapping("/")
    public String emptyForm(Model model) {
        model.addAttribute("encData", new Data());
        model.addAttribute("decData", new Data());
        return PAGE_INDEX;
    }

    @GetMapping("/encrypt")
    public String showEncryptedForm(Model model) {
        return emptyForm(model);
    }

    @PostMapping("/encrypt")
    public String encryptSubmit(@Valid @ModelAttribute("encData") Data encData, BindingResult result, @ModelAttribute("decData") Data decData, Model model) {
        if (!result.hasErrors()) {
            var se = ENCRYPTOR.stringEncryptor(encData.getPassword(), encData.getAlgorithm());
            String encryptedText = se.encrypt(encData.getIn());
            encData.setOut(encryptedText);
            decData.setIn(encData.getOut());
            decData.setOut("");
            LOGGER.info("Encrypted data: {}", encData.getOut());
        }
        return PAGE_INDEX;
    }

    @GetMapping("/decrypt")
    public String showDecryptedForm(@ModelAttribute("encData") Data encData, @ModelAttribute("decData") Data decData) {
        return PAGE_INDEX;
    }

    @PostMapping("/decrypt")
    public String decryptSubmit(@Valid @ModelAttribute("decData") Data decData, BindingResult result, @ModelAttribute("encData") Data encData, Model model) {
        if (!result.hasErrors()) {
            var se = ENCRYPTOR.stringEncryptor(decData.getPassword(), decData.getAlgorithm());
            decData.setOut(se.decrypt(decData.getIn()));
            encData.setIn(decData.getOut());
            encData.setOut("");
            LOGGER.info("Decrypted data: {}", decData.getOut());
        }
        return PAGE_INDEX;
    }
}

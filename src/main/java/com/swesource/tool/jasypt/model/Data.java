package com.swesource.tool.jasypt.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Data {

    @NotEmpty(message = "Field must not be empty")
    private String in;

    @NotEmpty
    private String algorithm;

    /**
     * Jasypt accepts ASCII characters only for the password
     */
    @Pattern(regexp = "\\A\\p{ASCII}*\\z", message = "ASCII characters only")
    @NotEmpty(message = "Field must not be empty")
    private String password;

    private String out;

    public String getIn() {
        return in;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }
}
